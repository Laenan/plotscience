# -*- coding: utf-8 -*-
"""
Script for loading with eprload over Matlab integration
"""

import sys
import os
import pandas as pd
import numpy as np
import matlab.engine

def load_eprload(file):
    eng = matlab.engine.start_matlab()
    mat_field,mat_spc,mat_param=eng.eprload(file,nargout=3) #give number of output parameters with nargout
    
    z_field = np.array(mat_field)
    z_spc = np.array(mat_spc)
    mylist_field = []
    mylist_spc = []
    for m in range(z_field.size):
        mylist_field.append(z_field[m].item())
    for m in range(z_spc.size):
        mylist_spc.append(z_spc[m].item())
        
    field=np.array(mylist_field)
    spc=np.array(mylist_spc)
    
    dataset = {}
    dataset['meta']='not implemented'
    dataset['data'] = pd.DataFrame({'x1': field,'y1': spc}) #rawdata['bckg'].col1.add(rawdata['bckg'].col3.apply(lambda x: complex(0,x)))})
    
    
    return dataset