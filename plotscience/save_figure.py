# -*- coding: utf-8 -*-
"""
This will save your figure. Can be modified by yaml-config file. When further keywords are specifies, these will overwrite the yaml file specification.

Keywords:
    config:
        Path Variable to .yaml config file
        
Created on Thu Apr 16 11:29:32 2020

@author: dbuecker
"""

from ruamel.yaml import YAML
from pathlib import Path
import inspect
import os
import warnings
import plotscience as ps
import matplotlib.pyplot as plt

def save_figure(fig,**kwargs):
    # This function will control the saving of the input fig.
    
    # List with all used keywords for yaml file. Not all can be specified in keywordlist from function
    keywords=['output_path','resolution','file_format','tight_box']
    
    ### Parse the kwargs:
    ## Optional load config file (keyword config) or default if not present. Has to be a Path object
    config_file = Path(kwargs.get('config',Path.joinpath(Path(__file__).parent.absolute(),'config_default.yaml')))
    # Load the config file
    yaml=YAML(typ='safe')   # default, if not specfied, is 'rt' (round-trip)
    docs=yaml.load_all(config_file)
    
    ## Load the config from the yaml file in variable: config
    for doc in docs:
        try:
            # puffer if key doesnt exist
            if doc['type']=='save':
                config_params=doc
                break
        except KeyError:
            warnings.warn('A document in the yaml file is missing the type attribute.\
                          \n This document will be skipped',UserWarning)
            continue
    
    try:
        config_params # check if variable is callable
    except NameError:
        warnings.warn('The yaml file is missing a document of type: save.\n Default config will be used!',UserWarning)
        docs=yaml.load_all(Path.joinpath(Path(__file__).parent.absolute(),'config_default.yaml'))
        for doc in docs:
            if doc['type']=='save':
                config_params=doc
                break
    
    ## Write some defaults.
    # Get defaults from default config for backup
    docs=yaml.load_all(Path.joinpath(Path(__file__).parent.absolute(),'config_default.yaml'))
    for doc in docs:
        if doc['type']=='save':
            default=doc
            break
    
    # Assign defaults if not set.
    
    for keyword in keywords:
        check_config(keyword,config_params,default)
        
    ## NOW defaults are setup, further kwargs import
    # Get the filename for saving, default: Callerfunction filename
    frame = inspect.stack()[1]
    # Get basename (with ext, then split. Will only remove the last extension)
    config_params['filename'] = kwargs.get('filename',os.path.splitext(os.path.basename(frame[0].f_code.co_filename))[0])
    
    config_params['resolution'] = kwargs.get('resolution',config_params['resolution'])
    
    config_params['file_format'] = kwargs.get('file_format',config_params['file_format'])

    ### Saving for every format
    # List of save function
    get_savefunction = {'png' : save_png,
                        'pgf' : save_pgf,
                        'tiff': save_tiff,
                        'pdf' : save_pdf,
                        'jpg' : save_jpg,
                        'svg' : save_svg,
                        }
    
    # Get save function and execute
    style_env=ps.get_style_params(**kwargs)
    for s_format in config_params['file_format']:
        try:
            save_function=get_savefunction[s_format]
        except KeyError as err:
            raise KeyError('There is a file_format requestes, that is not implemented. This key doesnt exists: '+err.args[0])
        save_function(fig,config_params,style_env)
    
    return

def check_config(keyword,config_params,default):
    try:
        config_params[keyword]
    except KeyError:
        config_params[keyword]=default[keyword]
    finally:
        return
    
### The save functions

# Save as png
def save_png(fig,config_params,style_env):
    import matplotlib as mpl
    extension='png'
    savepath=get_savepath(config_params,extension)
    # Using the Context of the basestyle is used, that the settings are correct for saving.
    with plt.style.context(style_env['base_style']):
        if config_params['tight_box']:
            fig.savefig(savepath,dpi=config_params['resolution'],bbox_inches='tight')
        else:
            fig.savefig(savepath,dpi=config_params['resolution'])
    return

# Save as svg
def save_svg(fig,config_params,style_env):
    extension='svg'
    savepath=get_savepath(config_params,extension)
    # Using the Context of the basestyle is used, that the settings are correct for saving.
    with plt.style.context(style_env['base_style']):
        if config_params['tight_box']:
            fig.savefig(savepath,dpi=config_params['resolution'],bbox_inches='tight')
        else:
            fig.savefig(savepath,dpi=config_params['resolution'])
    return

# Save as jpg
def save_jpg(fig,config_params,style_env):
    extension='jpg'
    savepath=get_savepath(config_params,extension)
    # Using the Context of the basestyle is used, that the settings are correct for saving.
    with plt.style.context(style_env['base_style']):
        if config_params['tight_box']:
            fig.savefig(savepath,dpi=config_params['resolution'],bbox_inches='tight')
        else:
            fig.savefig(savepath,dpi=config_params['resolution'])
    return

# Save as pdf
def save_pdf(fig,config_params,style_env):
    extension='pdf'
    savepath=get_savepath(config_params,extension)
    # Using the Context of the basestyle is used, that the settings are correct for saving.
    with plt.style.context(style_env['base_style']):
        if config_params['tight_box']:
            fig.savefig(savepath,dpi=config_params['resolution'],bbox_inches='tight')
        else:
            fig.savefig(savepath,dpi=config_params['resolution'])
    return

# Save as pgf
def save_pgf(fig,config_params,style_env):
    extension = 'pgf'
    savepath=get_savepath(config_params,extension)
    # Using the Context of the basestyle is used, that the settings are correct for saving.
    with plt.style.context(style_env['base_style']):
        if config_params['tight_box']:
            fig.savefig(savepath,dpi=config_params['resolution'],bbox_inches='tight')
        else:
            fig.savefig(savepath,dpi=config_params['resolution'])
    return

# Save as tiff
def save_tiff(fig,config_params,style_env):
    from PIL import Image
    from io import BytesIO
    
    extension = 'tiff'
    savepath=get_savepath(config_params,extension)
    
    # Using the Context of the basestyle is used, that the settings are correct for saving.
    with plt.style.context(style_env['base_style']):
        # convert to TIFF
        png1=BytesIO()
        if config_params['tight_box']:
            fig.savefig(png1,dpi=config_params['resolution'],bbox_inches='tight')
        else:
            fig.savefig(png1,dpi=config_params['resolution'])
            
        png2=Image.open(png1)
        png2.save(savepath,quality=100)
        png1.close()
        png2.close()
    
    return


def get_savepath(config_params,extension):
    savepath=Path.joinpath(Path(config_params['output_path']),config_params['filename']+'.'+extension)
    
    return savepath