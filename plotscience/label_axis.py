# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 17:10:41 2020
This will take an axis (not array of axes) and introduce the desired label. All font **kwargs are possible to manipulate.

@author: dbuecker
"""

def generic_label(ax,label,axis,**kwargs):
    if (axis=='y'):
        ax.set_ylabel(label,**kwargs)
    elif (axis=='x'):
         ax.set_xlabel(label,**kwargs)
    else:
        print('Axis has to be x or y specified')
        raise NameError

def x_time_us(ax,**kwargs):
    generic_label(ax,'Time t / $\mathrm{\mu}$s','x',**kwargs)
    
def x_time_ns(ax,**kwargs):
    generic_label(ax,'Time t / ns','x',**kwargs)
    
def x_distance_nm(ax,**kwargs):
    generic_label(ax,'Distance r / nm','x',**kwargs)
    
def x_wavelength_nm(ax,**kwargs):
    generic_label(ax,'Wavelength $\lambda$ / nm','x',**kwargs)
    
def x_frequency_MHz(ax,**kwargs):
    # only fontsize and labelsize are possible names.
    generic_label(ax,r"Frequency $\nu$ / MHz",'x',**kwargs)
    
def x_magnetic_field_T(ax,**kwargs):
    # only fontsize and labelsize are possible names.
    generic_label(ax,'Magnetic field B / T','x',**kwargs)
    
def y_magnetic_field_T(ax,**kwargs):
    # only fontsize and labelsize are possible names.
    generic_label(ax,'Magnetic field B / T','y',**kwargs)
    
def x_position_mm(ax,**kwargs):
    # only fontsize and labelsize are possible names.
    generic_label(ax,'Position / mm','x',**kwargs)
    
def y_intensityV_norm(ax,**kwargs):
    generic_label(ax,'Intensity V(t)/V(0)','y',**kwargs)
    
def y_intensityForm_norm(ax,**kwargs):
    generic_label(ax,'Intensity F(t)/F(0)','y',**kwargs)
    
def y_intensityV(ax,**kwargs):
    generic_label(ax,'Intensity V(t)','y',**kwargs)
    
def y_intensityEcho_au(ax,**kwargs):
    generic_label(ax,'Echo intensity a.u.','y',**kwargs)
    
def y_intensity(ax,**kwargs):
    generic_label(ax,'Intensity','y',**kwargs)
    
def y_intensityNorm(ax,**kwargs):
    generic_label(ax,'Intensity norm.','y',**kwargs)
    
def y_energy_mJ(ax,**kwargs):
    generic_label(ax,'Energy E / mJ','y',**kwargs)
    
def y_probabilityR(ax,**kwargs):
    generic_label(ax,'Probability P(r)','y',**kwargs)

    
def subplot_alphabet(fig,loc='left'):
    alphabet = ['A','B','C','D','E','F','G','H','I','J','K']
    if loc=="left":
        posx=0.05
    elif loc == "right":
        posx=0.95
    else:
        raise ValueError
            
    for i, ax in enumerate(fig.axes):
        #ax.set_title('%s'%alphabet[i],loc='left', fontweight='normal')
        ax.text(posx, .95, '%s'%alphabet[i], transform=ax.transAxes, ha=loc, va="top")