# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 15:23:28 2018

@author: dbuecker
"""
import numpy as np

def _figwidth(width=90,scale=1): # only use 1, 1.5 or 2 for scale!
    inch_mm = 25.4          # 1 inch = 25.4 mm
    if scale == 1:
        fig_width = width/inch_mm
    elif scale == 1.5:
        fig_width = (1.5*width+5)/inch_mm
    elif scale == 2:
        fig_width = (2*width+10)/inch_mm
    else :
        fig_width = 190/inch_mm #default to double column (full width)
        print('Attention! Unexpected figure scale. Defaulted to double column width')
    return fig_width

def _goldenmean():
    golden_mean = (np.sqrt(5.0)-1.0)/2.0            # Aesthetic ratio
    return golden_mean

def individual(width=None,height=None,scale=1):
    if width & height:
        fig_width=_figwidth(width=width,scale=scale)
        fig_height=_figwidth(width=height,scale=scale)
        fig_size = [fig_width,fig_height]
        return fig_size
    else:
        raise ValueError('Wrong/missing input for width and height')

def landscape(width=None,scale=None):
    if width:
        fig_width=_figwidth(width=width)
    elif scale:
        fig_width=_figwidth(scale=scale)
    else:
        fig_width=_figwidth()
    fig_height = fig_width*_goldenmean()              # height in inches
    fig_size = [fig_width,fig_height]
    return fig_size

def landscape_double(width=None,scale=None):
    if width:
        fig_width=_figwidth(width=width)
    elif scale:
        fig_width=_figwidth(scale=scale)
    else:
        fig_width=_figwidth()
    fig_height = fig_width*_goldenmean()              # height in inches
    fig_size = [2*fig_width,fig_height]
    return fig_size

def portrait(width=None,scale=None):
    if width:
        fig_width=_figwidth(width=width)
    elif scale:
        fig_width=_figwidth(scale=scale)
    else:
        fig_width=_figwidth()
    fig_height = fig_width/_goldenmean()              # height in inches
    fig_size = [fig_width,fig_height]
    return fig_size

def portrait_double(width=None,scale=None):
    if width:
        fig_width=_figwidth(width=width)
    elif scale:
        fig_width=_figwidth(scale=scale)
    else:
        fig_width=_figwidth()
    fig_height = fig_width/_goldenmean()              # height in inches
    fig_size = [fig_width,fig_height*2]
    return fig_size