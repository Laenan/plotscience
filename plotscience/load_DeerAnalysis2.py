# -*- coding: utf-8 -*-
"""
Created on Fri Feb 07 18:28:48 2020

This imports data from DeerAnalysis2. The datastructure is designed similar to Deeranalysis, but w/o _res.txt file. There may be a datafile for analysis parameters.


@author: dbuecker
"""
import sys
import os
import pandas as pd
import scipy.io as spio


"""
data structure in general:
    experiment-dataset (dict):
        rawdata (a pandas array, rawdata in SI, complex)
        data (a pandas array, momentary data, complex)
        meta (meta informations)
        analysis (information that is needed for analysis/plotting, like fits etc)
"""

def load_DeerAnalysis2(filepath):
    # Consistens checks for filename.
    (path,filename_res)=os.path.split(filepath)
    if not filename_res.endswith('.mat'):
        sys.exit('Too bad, {} is not a DeerAnalysis2 .mat-file.'.format(filename_res))
    else:
        filename=filename_res[:-4] # cut the .mat ending
        print('Start import {} ...'.format(filename))
        DAdataraw = spio.loadmat(filepath,variable_names=['DAdata'])

        DAdata=DAdataraw['DAdata']

    # import metadata
    dataset = {}
    dataset['meta'] = {}
    dataset['meta']['datatype']='deer'
    dataset['meta']['zerotime']=DAdata['meta'][0,0]['zerotime'][0,0]
    dataset['meta']['bckgstart']=['None']
    dataset['meta']['cutoff']=DAdata['meta'][0,0]['cutoff'][0,0]
    dataset['meta']['modulation_depth']=DAdata['meta'][0,0]['modulation_depth'][0,0]
    dataset['meta']['analysis']=DAdata['meta'][0,0]['analysis'].item()[0]
    
    try:
        if dataset['meta']['analysis']=='parametric':
            dataset['meta']['model_parameter']=DAdata['model'][0,0]
    except:
        Warning
    
    # Assign data
    dataset['trace']=pd.DataFrame({'time': DAdata['time'][0,0][0],'intensity': DAdata['deer_trace_real'][0,0][0]+1j*DAdata['deer_trace_imag'][0,0][0],'fit':DAdata['deer_trace_fit'][0,0][0],'background_fit':DAdata['deer_trace_background'][0,0][0]})
    dataset['form_factor']=pd.DataFrame({'time':DAdata['time'][0,0][0],'intensity':DAdata['form_factor'][0,0][0],'fit':DAdata['form_factor_fit'][0,0][0]})
    dataset['distance']=pd.DataFrame({'distance':DAdata['distance'][0,0][0],'intensity':DAdata['distance_probability'][0,0][0]})
    dataset['spectrum']= pd.DataFrame({'frequency':DAdata['frequency'][0,0][0],'intensity':DAdata['dipolar_spectrum'][0,0][0],'fit':DAdata['dipolar_spectrum_fit'][0,0][0]})
    
    # Check if there are CIs and add them
    try:
        dataset['distance']['probability_lower']=DAdata['distance_probability_lower'][0,0][0]
        dataset['distance']['probability_upper']=DAdata['distance_probability_upper'][0,0][0]
    except:
        Warning
        
    # Check if there is a model of type WLCg
    try:
        if DAdata['model'][0,0]['model'].item()[0] == 'wlcg':
            dataset['meta']['persistence_length'] = DAdata['model'][0,0]['persistence'].item()[0][0]
            dataset['meta']['contour_length'] = DAdata['model'][0,0]['contour'].item()[0][0]
            dataset['meta']['wlc_gaussstd'] = DAdata['model'][0,0]['gaussstd'].item()[0][0]
    except:
        Warning
    
    print('{} imported.'.format(filename))
    
    return dataset

if __name__== "__main__":
    print('What do you expect? I\'m just loading your shit.')