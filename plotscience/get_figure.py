# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 10:41:17 2020

@author: dbuecker
"""

import matplotlib.pyplot as plt
import plotscience as ps

def get_figure(*argv,**kwargs):
    # Will generate(and return) a figure.
    
    # Load and apply the basestyle
    params=ps.get_style_params(*argv,**kwargs)
       
    ### Generate figure with altered base_style
    
    with plt.style.context(params['base_style']):
        fig=plt.figure(figsize=params['figsize'])

    
    return fig