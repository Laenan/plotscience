# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 10:41:17 2020

@author: dbuecker
"""

from ruamel.yaml import YAML
from pathlib import Path
import warnings
import matplotlib.pyplot as plt
import numpy as np
import plotscience as ps
from matplotlib import rc_params_from_file
import importlib # To use import from string for color cycle

def get_style_params(*argv,**kwargs):
    # Will generate(and return) a figure.
    
    # List with all used keywords for yaml file. Not all can be specified in keywordlist from function
    keywords=['base_style','width','orientation','ratio','color_cycle']
    
    ### Parse the kwargs:
    ## Optional load config file (keyword config) or default if not present. Has to be a Path object
    config_file = Path(kwargs.get('config',Path.joinpath(Path(__file__).parent.absolute(),'config_default.yaml')))
    # Load the config file
    yaml=YAML(typ='safe')   # default, if not specfied, is 'rt' (round-trip)
    docs=yaml.load_all(config_file)
    
    ## Load the config from the yaml file in variable: config
    for doc in docs:
        try:
            # puffer if key doesnt exist
            if doc['type']=='figure':
                config_params=doc
                break
        except KeyError:
            warnings.warn('A document in the yaml file is missing the type attribute.\
                          \n This document will be skipped',UserWarning)
            continue
    
    try:
        config_params # check if variable is callable
    except NameError:
        warnings.warn('The yaml file is missing a document of type: figure.\n Default config will be used!',UserWarning)
        docs=yaml.load_all(Path.joinpath(Path(__file__).parent.absolute(),'config_default.yaml'))
        for doc in docs:
            if doc['type']=='figure':
                config_params=doc
                break    
    
    ## Write some defaults.
    # Get defaults from default config for backup
    docs=yaml.load_all(Path.joinpath(Path(__file__).parent.absolute(),'config_default.yaml'))
    for doc in docs:
        if doc['type']=='figure':
            default=doc
            break
    
    # Assign defaults if not set.
    
    for keyword in keywords:
        check_config(keyword,config_params,default)
     
    yaml=YAML(typ='safe')   # default, if not specfied, is 'rt' (round-trip)    
    ## Overwrite with special parameters from figure, if input was given
    
    if len(argv) > 1:
        raise ValueError('To many input arguments.')
    elif len(argv) == 1:
        # Overwrite all found keywords in yaml file.
        file_config=yaml.load(argv[0])
        for key in file_config:
            config_params[key]=file_config[key]
    
    ### Begin setup
    
    
    ## Calculate figsize
    
    # Update ratio if golden mean was set
    if config_params['ratio'] == 'golden':
        config_params['ratio'] = (np.sqrt(5.0)-1.0)/2.0   # Golden mean Aesthetic ratio
    
    # Calculate height
    if config_params['orientation'] == 'landscape':
        config_params['height']=config_params['width']*config_params['ratio']
    elif config_params['orientation'] == 'portrait':
        config_params['height']=config_params['width']/config_params['ratio']
    else:
        raise ValueError('Input orientation wasnt landscape or portrait!')
        
    inch_mm = 25.4   # 1 inch = 25.4 mm
    config_params['figsize']=[config_params['width']/inch_mm,config_params['height']/inch_mm]

    
    ### Create figure
    ## Load rcParams
    
    # One could use the rcParam file to only make the figure, instead of updating the complete one.
    
    if config_params['base_style'] == 'default':
        base_style=ps.style.default
    elif config_params['base_style'] == 'none':
        base_style={}
        
    else:
        try:
            base_style_path=Path(config_params['base_style'])
        except:
            raise ValueError('Base style wasnt a valid path, default or None.')
        
        if base_style_path.is_file():
            base_style=rc_params_from_file(base_style_path.resolve().__str__())
        else:
            raise ValueError('Could not load the named base_style.')
    
    base_style = remove_blacklisted_style_params(base_style)
    config_params['base_style']=base_style # overwrite property for output.
    
        
    ## Set color cycle
    if config_params['color_cycle']:
        if config_params['color_cycle']['type'] == 'default':
            pass
        else:
            # make a switch for diff cyclers
            get_cyclerfunction = {'tab' : cycle_tab,
                                  'dark' : cycle_dark,
                                  'safe': cycle_safe,
                                  'vivid' : cycle_vivid,
                                  }
            
            try:
                cycler_function=get_cyclerfunction[config_params['color_cycle']['type']]
            except KeyError as err:
                raise KeyError('The requested color-cycle is not implemented. This key doesnt exists: '+err.args[0])
            
            # Select the cycler
            cycler_function(config_params['color_cycle'])
    
    # return the basestyle. Not sure, if color cycle will be set permanently, but it should. Maybe has to be included in basestyle.
    return config_params

def check_config(keyword,config_params,default):
    try:
        config_params[keyword]
    except KeyError:
        config_params[keyword]=default[keyword]
    finally:
        return
    
def remove_blacklisted_style_params(d):
    # stolen from matplotlib, doubles with the init
    import warnings
    from matplotlib.cbook import MatplotlibDeprecationWarning
    o = {}
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", MatplotlibDeprecationWarning)
        for key, val in d.items():
            if key in ps.STYLE_BLACKLIST:
                pass
            else:
                o[key] = val
                
    del warnings
    return o


def cycle_tab(cycle):
    import palettable
    from cycler import cycler
    cycle_selector = { 20: palettable.tableau.Tableau_20,
                       10: palettable.tableau.Tableau_10,
                      }
    try:
        plt.rc('axes', prop_cycle=cycler(color=cycle_selector[cycle['number']].mpl_colors))
    except KeyError:
        raise('That number is not implemented.')
    
    del cycler
    del palettable 
    return

def cycle_dark(cycle):
    import palettable
    from cycler import cycler
    cycle_selector = { 3: palettable.colorbrewer.qualitative.Dark2_3,
                       4: palettable.colorbrewer.qualitative.Dark2_4,
                       5: palettable.colorbrewer.qualitative.Dark2_5,
                       6: palettable.colorbrewer.qualitative.Dark2_6,
                       7: palettable.colorbrewer.qualitative.Dark2_7,
                       8: palettable.colorbrewer.qualitative.Dark2_8,
                      }
    try:
        plt.rc('axes', prop_cycle=cycler(color=cycle_selector[cycle['number']].mpl_colors))
    except KeyError:
        raise('That number is not implemented.')
    
    del cycler
    del palettable 
    
    return

def cycle_vivid(cycle):
    import palettable
    from cycler import cycler
    cycle_selector = { 2: palettable.cartocolors.qualitative.Vivid_2,
                       3: palettable.cartocolors.qualitative.Vivid_3,
                       4: palettable.cartocolors.qualitative.Vivid_4,
                       5: palettable.cartocolors.qualitative.Vivid_5,
                       6: palettable.cartocolors.qualitative.Vivid_6,
                       7: palettable.cartocolors.qualitative.Vivid_7,
                       8: palettable.cartocolors.qualitative.Vivid_8,
                       9: palettable.cartocolors.qualitative.Vivid_9,
                       10: palettable.cartocolors.qualitative.Vivid_10,
                      }
    try:
        plt.rc('axes', prop_cycle=cycler(color=cycle_selector[cycle['number']].mpl_colors))
    except KeyError:
        raise('That number is not implemented.')
    
    del cycler
    del palettable 
    
    return

def cycle_safe(cycle):
    import palettable
    from cycler import cycler
    cycle_selector = { 2: palettable.cartocolors.qualitative.Safe_2,
                       3: palettable.cartocolors.qualitative.Safe_3,
                       4: palettable.cartocolors.qualitative.Safe_4,
                       5: palettable.cartocolors.qualitative.Safe_5,
                       6: palettable.cartocolors.qualitative.Safe_6,
                       7: palettable.cartocolors.qualitative.Safe_7,
                       8: palettable.cartocolors.qualitative.Safe_8,
                       9: palettable.cartocolors.qualitative.Safe_9,
                       10: palettable.cartocolors.qualitative.Safe_10,
                      }
    try:
        plt.rc('axes', prop_cycle=cycler(color=cycle_selector[cycle['number']].mpl_colors))
    except KeyError:
        raise('That number is not implemented.')
    
    del cycler
    del palettable    
    return