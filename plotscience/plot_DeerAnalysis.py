# -*- coding: utf-8 -*-
"""
The package can create an overview image for DeerAnalysis and the single plots.
Input:
    pandas data array for DeerAnalysis

Created on Thu Jan 11 17:47:52 2018

@author: dbuecker
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import plotscience as ps
import numpy as np
import sys

def overview(data):
    _isdeer(data)
    
    with plt.style.context([ps.style.default]):
        mpl.rcParams['font.size']=ps.style.default['font.size']/2
        fig,axes=plt.subplots(2, 2)
    
    _, _=trace(data,ax=axes[0,0])
    _, _=form_factor(data,ax=axes[1,0])
    _, _=spectrum(data,ax=axes[0,1])
    _, _=distance(data,ax=axes[1,1])
    
    
    return fig, axes

def trace(data,ax=False,indicators=True):
    _isdeer(data)
    
    if ax:
        axes = ax
        fig = None
    else:
        with plt.style.context([ps.style.default]):
            fig,axes=plt.subplots(1,1)
        
    with plt.style.context([ps.style.default,ps.style.experiment]):
        axes.plot(data['trace']['time'],np.real(data['trace']['intensity']),linestyle='-',marker='None')
        #np.abs(data['trace']).plot(x='time',y='intensity',kind='line',ax=axes) # neglect complex for this plot
    with plt.style.context([ps.style.default,ps.style.fit]):
        data['trace'].plot(x='time',y='bckg',kind='line',ax=axes)
    
    if indicators:
        axes.axvspan(data['trace']['time'][0],0,alpha=0.7,facecolor = axes.lines[0].get_color())
        axes.axvspan(data['meta']['bckgstart'],data['meta']['cutoff'],alpha=0.3,facecolor = axes.lines[0].get_color())
        axes.axvspan(data['meta']['cutoff'],data['trace']['time'].iloc[-1],alpha=0.7,facecolor = axes.lines[0].get_color())
    
    
    axes.xaxis.set_label_text('Time ($\mathrm{\mu}$s)')
    axes.yaxis.set_label_text('Intensity V(t) (norm.)')
    
    with plt.style.context([ps.style.default]):
        plt.legend(axes.lines,['data','background'])
    
    return fig,axes

def form_factor(data,ax=False,exp=True,fit=True):
    _isdeer(data)
    
    if ax:
        axes = ax
        fig = None
    else:
        with plt.style.context([ps.style.default]):
            fig,axes=plt.subplots(1,1)
    
    if exp==True:
        with plt.style.context([ps.style.default,ps.style.experiment]):
            data['form_factor'].plot(x='time',y='intensity',kind='line',ax=axes,style='-')
            #ax.plot(data['form_factor'].time,data['form_factor'].intensity)
            
    if fit==True:
        with plt.style.context([ps.style.default,ps.style.fit]):
            data['form_factor'].plot(x='time',y='fit',kind='line',ax=axes)
    
    
    axes.xaxis.set_label_text('Time ($\mathrm{\mu}$s)')
    axes.yaxis.set_label_text('V(t)/V(0)')
    
    with plt.style.context([ps.style.default]):
        plt.legend(axes.lines,['data','fit'])
    
    return fig,axes

def spectrum(data,ax=False,scale=True):
    _isdeer(data)
    
    if ax:
        axes = ax
        fig = None
    else:
        with plt.style.context([ps.style.default]):
            fig,axes=plt.subplots(1,1)
    
    with plt.style.context([ps.style.default,ps.style.experiment]):
        data['spectrum'].plot(x='frequency',y='intensity',style='-',kind='line',ax=axes)
    with plt.style.context([ps.style.default,ps.style.fit]):
        data['spectrum'].plot(x='frequency',y='fit',kind='line',ax=axes)
    
    
    # set x-axis to area of interest:
    if scale:
        treshold = 0.1
        lowerx = data['spectrum']['frequency'][round((len(data['spectrum']['fit'])-3*sum(data['spectrum']['fit'] > treshold))/2)]
        upperx = data['spectrum']['frequency'][round(((len(data['spectrum']['fit'])-3*sum(data['spectrum']['fit'] > treshold))/2)+3*sum(data['spectrum']['fit'] > treshold))]
        axes.set_xlim([lowerx,upperx])
    
    axes.xaxis.set_label_text('Frequency (MHz)')
    axes.yaxis.set_label_text('Intensity (a.u.)')
    
    with plt.style.context([ps.style.default]):
        plt.legend(axes.lines,['data','fit'])

    return fig,axes

def distance(data,ax=False,validation=False):
    _isdeer(data)
    
    if ax:
        axes = ax
        fig = None
    else:
        with plt.style.context([ps.style.default]):
            fig,axes=plt.subplots(1,1)
            
    

    # plot it
    with plt.style.context([ps.style.default,ps.style.experiment]):
            data['distance'].plot(x='distance',y='intensity',style='-',kind='line',ax=axes)
            
    # plot validation, validation is typically always plotted, if there hasn't been done a validation, then the colored area will not exist, beacue lower-limit=upper-limit
    if validation:
        with plt.style.context([ps.style.default,ps.style.experiment]):
            axes.fill_between(data['distance']['distance'],data['distance']['vali-lower'],data['distance']['vali-upper'],alpha=0.3,facecolor='k')
    
    #axes.get_yaxis().set_visible(False)
    axes.set_yticks([])
    
    axes.xaxis.set_label_text('Distance (nm)')
    axes.yaxis.set_label_text('Probability (a.u.)')
    
    # Remove Legend
    _legend = axes.legend()
    _legend.remove()
    
    return fig,axes

#plot validation for the "old-school" validation method with the extra window
def validation(data,distr_set=0,ax=False):
    _isdeer(data)
    
    if ax:
        axes = ax
        fig = None
    else:
        with plt.style.context([ps.style.default]):
            fig,axes=plt.subplots(1,1)



    if distr_set>=np.size(data['validation_sets']['depth_vals']) | distr_set <0:
            sys.exit('Your index is out-of range')
    else:
        with plt.style.context([ps.style.default,ps.style.experiment]):
            axes.plot(data['distance']['distance'],data['validation_sets']['trial_distr'][distr_set,:])
         
    with plt.style.context([ps.style.default,ps.style.experiment]):
        axes.fill_between(data['distance']['distance'],data['validation_sets']['vali-lower'],data['validation_sets']['vali-upper'],alpha=0.3,facecolor='k')
    
    
    #axes.get_yaxis().set_visible(False)
    axes.set_yticks([])
    
    axes.xaxis.set_label_text('Distance (nm)')
    axes.yaxis.set_label_text('Probability (a.u.)')
    
    # Remove Legend
    #_legend = axes.legend()
    #_legend.remove()

    return fig,axes

def validation_form_factor(data,distr_set=0,ax=False):
    _isdeer(data)
    
    if ax:
        axes = ax
        fig = None
    else:
        with plt.style.context([ps.style.default]):
            fig,axes=plt.subplots(1,1)

    with plt.style.context([ps.style.default,ps.style.experiment]):
        data['form_factor'].plot(x='time',y='intensity',kind='line',ax=axes,style='-')
        
    if distr_set>=np.size(data['validation_sets']['depth_vals']) | distr_set <0:
            sys.exit('Your index is out-of range')
    else:
        with plt.style.context([ps.style.default,ps.style.fit]):
            axes.plot(data['form_factor']['time'],data['validation_sets']['trial_sim'][distr_set])
    
    
    axes.xaxis.set_label_text('Time ($\mu$s)')
    axes.yaxis.set_label_text('V(t)/V(0)')
    
    with plt.style.context([ps.style.default]):
        plt.legend(axes.lines,['data','fit'])
    
    return fig,axes


def _isdeer(data):
    if data['meta']['datatype']=='deer':
        result = True
    else:
        raise TypeError('Data is not of type \'deer\'!')
        result = False
    
    return result

def set_spectrum_threshold(ax,deer,threshold=0.1):
    # Helper function will set the limit on x axis in such way, that unnecessary baseline is removed
    import numpy as np
    
    intensity=deer['spectrum']['intensity']
    x=deer['spectrum']['frequency']
    cutoff = intensity.max()*threshold
    
    diff_idx=np.abs(len(x)/2-(np.abs(intensity-cutoff)).idxmin())
    lowerx = deer['spectrum']['frequency'][len(x)/2-diff_idx]
    upperx = deer['spectrum']['frequency'][len(x)/2+diff_idx]
    ax.set_xlim([lowerx,upperx])
    