# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 11:08:08 2018

@author: andi
"""

import plotscience as ps
import scipy.signal as scs
import numpy as np
import matplotlib.pyplot as plt



def MNR(data,n,ax=False,cutoff=0):
    
    trace=data['form_factor'].intensity.real[cutoff:]
    
    n_start=int(np.round(trace.size/8))  #only use last 7/8 of the deer trace
    n_start=0
    trace_filtered=scs.savgol_filter(trace,n,1)   
    
    noise=np.std(trace[n_start:]-trace_filtered[n_start:])
    
    signal=data['meta']['modulation_depth']
    
    if ax!=False:
        with plt.style.context([ps.style.default]):
            ax.plot(data['form_factor'].time,data['form_factor'].intensity)
            ax.plot(data['form_factor'].time[cutoff:],trace_filtered)
            
    return signal/noise
    
