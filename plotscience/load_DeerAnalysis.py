# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 13:28:48 2018

@author: dbuecker
"""
import sys
import os
import pandas as pd
import re
import matlab.engine
import numpy as np

"""
data structure in general:
    experiment-dataset (dict):
        rawdata (a pandas array, rawdata in SI, complex)
        data (a pandas array, momentary data, complex)
        meta (meta informations)
        analysis (information that is needed for analysis/plotting, like fits etc)
"""

def load_DeerAnalysis(filepath):
    # parse the input. Input must be a '*_res.txt'-file
    
    # Consistens checks for filename.
    (path,filename_res)=os.path.split(filepath)
    if not filename_res.endswith('_res.txt'):
        sys.exit('Too bad, {} is not a DeerAnalysis _res.txt-file.'.format(filename_res))
    else:
        filename=filename_res[:-8] # cut the _res.txt ending
        print('Start reading {} ...'.format(filename))
     

    
    # read the res file
    dataset = {}
    dataset['meta'] = read_res(path,filename)
    
    # make a list of valid idents:
    # known idents from DeerAnalysis2016
    knownidents = ['_bckg','_distr','_spc','_fit','_Lcurve']
    idents = []
    # prepare only idents, where a file is present for loading-list
    for kident in knownidents:
        if os.path.isfile(os.path.join(path,filename+kident+'.dat')):
            idents.append(kident)
    # remove Lcurve if not needed.        
    if not dataset['meta']['Tikhonov']:
        try:
            idents.remove('_Lcurve')
        except:
            pass
        
    # Load data from all specified idents:
    rawdata = {}
    for ident in idents:
        rawdata[ident[1:]]=import_dat(path,filename,ident)
    
    # Write the data:
    dataset['data']=pd.DataFrame({'time': rawdata['bckg'].col0-rawdata['bckg'].col0[0],'intensity': rawdata['bckg'].col1.add(rawdata['bckg'].col3.apply(lambda x: complex(0,x)))})
    dataset['meta']['unit']=['s','a.u.']
    
    
    dataset['trace']=pd.DataFrame({'time': rawdata['bckg'].col0,'intensity': rawdata['bckg'].col1.add(rawdata['bckg'].col3.apply(lambda x: complex(0,x))),'bckg':rawdata['bckg'].col2})
    dataset['form_factor']=pd.DataFrame({'time':rawdata['fit'].col0,'intensity':rawdata['fit'].col1,'fit':rawdata['fit'].col2})
    
    
    if 'col2' in rawdata['distr'].columns:
        # col2 and col3 include the limits of the validation, if no validation has been done col1=col2=col3
        dataset['distance']= pd.DataFrame({'distance':rawdata['distr'].col0,'intensity':rawdata['distr'].col1,'vali-lower':rawdata['distr'].col2,'vali-upper':rawdata['distr'].col3})
    else:
        #for down compatiblity of model based fits
        dataset['distance']= pd.DataFrame({'distance':rawdata['distr'].col0,'intensity':rawdata['distr'].col1})   
    
    #for the .mat file
    validation_ident_mat='_validation_sets'
    file_validation=os.path.join(path,filename+validation_ident_mat+'.mat')

    if os.path.isfile(file_validation):
        eng = matlab.engine.start_matlab()
        matlab_validation_import=eng.load(file_validation,nargout=1) #give number of output parameters with nargout
        for key_tmp in matlab_validation_import.keys():
            matlab_validation_import[key_tmp]=np.array(matlab_validation_import[key_tmp])
        dataset[validation_ident_mat[1:]]=matlab_validation_import
        
        #define lower and upper limit
        dataset[validation_ident_mat[1:]]['vali-lower']=np.min(dataset['validation_sets']['trial_distr'],0)
        dataset[validation_ident_mat[1:]]['vali-upper']=np.max(dataset['validation_sets']['trial_distr'],0)
        
        #update the third and fourth column in analysis/distr
        dataset['distance']['vali-lower']=dataset[validation_ident_mat[1:]]['vali-lower']
        dataset['distance']['vali-upper']=dataset[validation_ident_mat[1:]]['vali-upper']
    
    dataset['spectrum']= pd.DataFrame({'frequency':rawdata['spc'].col0,'intensity':rawdata['spc'].col1,'fit':rawdata['spc'].col2})
    
    if  dataset['meta']['Tikhonov']:
        # Writing L-curve if this was analysed by tikhonov. 
        try:
            dataset['lcurve'] = pd.DataFrame({'rho':rawdata['Lcurve'].col0,'eta':rawdata['Lcurve'].col1,'regpar':rawdata['Lcurve'].col2})
        except:
            pass
        
    return dataset


def read_res(filepath,filename):
    """ function reads the _res.txt file of a DeerAnalysis analyse.
    Imports just some meta data, but more can be implemented. Please be careful, because layout of _res.txt file changes with type of analyis"""
    general={}
    general['datatype']='deer'
    try:
        f = open(os.path.join(filepath,filename+'_res.txt'),'r')
    except FileNotFoundError:
        sys.exit('File {} not found.'.format(filename+'_res.txt'))
    else:
        lines=f.read()
        f.close()
        
    # search for Timeshift, which is the zero time:
#    Timeshift_start=lines.find('Time shift: ')+12
#    Timeshift_end=lines.find(' ns\nCutoff at')
#    general['zerotime']=float(lines[Timeshift_start:Timeshift_end])/1000 # read in ns, so /1000 for us  
    exp=r'Time shift[ :]+([\d.,]+)[ ]+ns'
    temp=re.search(exp,lines)
    general['zerotime']=float(temp.group(1))/1000    #ns -> us
    print("Time shift: {0} us\n".format(general['zerotime']))
    
    
    # search for Backgroundstart, which is the Starting at (type is in advance):
    exp=r'Starting at[ :]+([\d.,e+]+)[ ]+ns'
    temp=re.search(exp,lines)
    general['bckgstart']=float(temp.group(1))/1000
    print("Background start read out as: {0} us\n".format(general['bckgstart']))

    # search for Cutoff, which is Cutoff at:
    exp=r'Cutoff at[ :]+([\d.,]+)[ ]+ns'
    temp=re.search(exp,lines)
    general['cutoff']=float(temp.group(1))/1000
    print("Cutoff read out as: {0} us\n".format(general['cutoff']))    
    
    
    # search for Mod Depth, which is Cutoff at:
    exp=r'Modulation depth[ :]+([\d.,]+)'
    temp=re.search(exp,lines)
    general['modulation_depth']=float(temp.group(1))
    print("Modulation depth read out as: {0} us\n".format(general['modulation_depth']))   
    
    # search for r.m.s. error of fit (not background!):
    exp=r'r.m.s. error of fit[ :]+([\d.,]+)'
    temp=re.search(exp,lines)
    general['rms_error_fit']=float(temp.group(1))
    print("r.m.s. error of fit: {0} us\n".format(general['modulation_depth']))  

    
    # search for tikhonov parameter if
    if lines.find('Regularization parameter: ') < 0:
        print('Dataset NOT analysed with Tikhonov')
        general['Tikhonov']=False
    else:
        exp=r'Regularization parameter[ :]+([\d.,]+)'
        temp=re.search(exp,lines)
        general['alpha1']=float(temp.group(1)) 
        general['Tikhonov']=True
        general['analysis']='tikhonov'
        print('Dataset analysed with Tikhonov')
        print("Regularization parameter read out as: {0}\n".format(general['alpha1']))
        
    # serach for model analysis if present:
    if lines.find('Model parameters for ') < 0:
        print('Dataset NOT analysed model based')
        general['model']=False
    else:
        exp=r'Model parameters for[ ]+([\S]+)'
        temp=re.search(exp,lines)
        general['model']=temp.group(1)
        general['analysis']='model'
        print('Dataset analysed with model {0}'.format(general['model']))
        
        exp=r'([\S]+)[ = ]+([\d.,e-]+)[, ]+(fitted|fixed)'
        #TODO: Kill ending ',' from results. Then correct this in the saving loop [:-1]
        temp=re.findall(exp,lines)
        general['modelparameter']={}
        print('The following parameters were used:')
        for expression in temp:
            general['modelparameter'][expression[0]]=float(expression[1][:-1])
            print('{0} = {1}'.format(expression[0],expression[1][:-1]))
    
    return general

def import_dat(path_input,filename,ident):
    location=os.path.join(path_input,filename+ident+'.dat')
    rawdata=pd.read_csv(location,header=None,prefix='col',delim_whitespace=True)
    return rawdata

if __name__== "__main__":
    print('What do you expect? I\'m just loading your shit.')
    