# -*- coding: utf-8 -*-


# This should add everything to __all__ but maybe to much, check this again!
import pkgutil
import os

__all__ = []
for loader, module_name, is_pkg in  pkgutil.walk_packages(__path__):
    __all__.append(module_name)
    module = loader.find_module(module_name).load_module(module_name)
    exec('%s = module' % module_name)
    

# Load Modules to top layer.
from .load_DeerAnalysis import load_DeerAnalysis
from .load_DeerAnalysis2 import load_DeerAnalysis2
from .load_UVVIS import load_UVVIS
from .load_eprload import load_eprload
from .save_figure import save_figure
from .get_figure import get_figure
from .get_axis import get_axis
from .get_style_params import get_style_params

# A list of rcParams that should not be applied from styles
STYLE_BLACKLIST = {
    'interactive', 'backend', 'backend.qt4', 'webagg.port', 'webagg.address',
    'webagg.port_retries', 'webagg.open_in_browser', 'backend_fallback',
    'toolbar', 'timezone', 'datapath', 'figure.max_open_warning',
    'savefig.directory', 'tk.window_focus', 'docstring.hardcopy', 
    'text.latex.unicode', 'examples.directory','savefig.frameon','verbose.fileo','verbose.level',
    'date.epoch','figure.raise_window','animation.avconv_args','animation.avconv_path',
    'animation.html_args','keymap.all_axes','savefig.jpeg_quality','text.latex.preview'}


def _remove_blacklisted_style_params(d):
    # stolen from matplotlib
    import warnings
    from matplotlib.cbook import MatplotlibDeprecationWarning
    o = {}
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", MatplotlibDeprecationWarning)
        for key, val in d.items():
            if key in STYLE_BLACKLIST:
                pass
            else:
                o[key] = val
                
    del warnings
    return o

def _reset_styles():
    """ Function resets the styles and/or generates the initial ones"""
    from matplotlib import rc_params_from_file
    styles_path = os.path.abspath(os.path.join(os.path.dirname(__file__),'styles'))
    default_path = os.path.join(styles_path,'default.mplstyle')
    experiment_path = os.path.join(styles_path,'experiment.mplstyle')
    fit_path = os.path.join(styles_path,'fit.mplstyle')
    
    default = _remove_blacklisted_style_params(rc_params_from_file(default_path))
    experiment = _remove_blacklisted_style_params(rc_params_from_file(experiment_path))
    fit = _remove_blacklisted_style_params(rc_params_from_file(fit_path))
    
    del rc_params_from_file
    return default,experiment,fit

# Load style if not yet imported. This is not mandatory, but makes behaviour clear.
if style.default == None:
    style.default,style.experiment,style.fit = _reset_styles()