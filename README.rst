INSTALLATION 
=======================
ANACONDA 
---------------
In Anaconda Prompt, change to folder where package is located::

        activate installation-env   # change to your environement
        pip install -e .            # -e statement will establish a symlink