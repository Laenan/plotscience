from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='plotscience',
      version='0.2',
      description='Tools to plot scientific data (EPR) with matplotlib. Data import, visualization and more.',
      long_description=readme(),
      url='https://gitlab.com/Laenan/plotscience',
      author='Dennis Buecker <dennis.buecker@uni-konstanz.de>, Andreas Scherer <andreas.2.scherer@uni-konstanz.de>',
      author_email='dennis.buecker@uni-konstanz.de',
      license='MIT',
      packages=['plotscience'],
      install_requires=[
              'pandas',
              'numpy',
              'matplotlib',
              'pathlib',
              'palettable', # for color styles
              'ruamel.yaml' # for the config files YAML v1.2
              ],
      python_requires='>=3',
      zip_safe=False)