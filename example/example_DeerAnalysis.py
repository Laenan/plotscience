# -*- coding: utf-8 -*-

import plotscience as ps
import matplotlib.pyplot as plt

data = ps.load_DeerAnalysis('./data/DeerAnalysis_res.txt')


fig,axes = ps.plot_DeerAnalysis.trace(data)
fig,axes = ps.plot_DeerAnalysis.form_factor(data)
fig,axes = ps.plot_DeerAnalysis.spectrum(data)
fig,axes = ps.plot_DeerAnalysis.distance(data)

fig,axes = ps.plot_DeerAnalysis.overview(data)

plt.show()