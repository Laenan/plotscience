# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 20:43:36 2021

Example file for plotting with the layout from plotscience
Also shows labeling and subplot labelling

@author: dbuecker
"""

#import scipy.io as spio
import pandas as pd
import numpy as np
import matplotlib as mpl

from matplotlib import pyplot as plt
import plotscience as ps
from pathlib import Path

inp = """\
    # Here you can make adjustments to just this plot.
    orientation: landscape # portrait or landscape    
    """

    
# Plot
fig=ps.get_figure(inp,config=Path('./config_example.yaml')) # This will take the figure
params = ps.get_style_params(inp,config=Path('./config_example.yaml'))

axes=[]
with plt.style.context(params['base_style']):
    ax3=fig.add_subplot(223)
    ax4=fig.add_subplot(224)
    
    ax1=fig.add_subplot(221)
    ax2=fig.add_subplot(222)
    
    axes = [[ax1,ax2],[ax3,ax4]]
    
    for i in range(10):
        ax1.plot(np.array([0,1]),np.array([0,1])+i)
        ax2.plot(np.array([1,0]),np.array([0,1])+i)
        
    ps.label_axis.x_wavelength_nm(ax1)
    ps.label_axis.y_energy_mJ(ax1)
    
    ps.label_axis.subplot_alphabet(fig,loc='left')
    
    
ps.save_figure(fig,config=Path('./config_example.yaml'))
    
